package me.taucu.commandblocker;

import java.util.Collection;
import java.util.List;

public interface Config {

    public void setDefaults(Config config);

    public Object get(String key);

    public Config getSection(String path);

    public Collection<String> getKeys();

    public String getString(String key);

    public String getString(String key, String defaultValue);

    public int getInt(String key);

    public double getDouble(String key);

    public List<String> getStringList(String key);

    public boolean contains(String key);

}
