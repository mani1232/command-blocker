package me.taucu.commandblocker.filters;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import me.taucu.commandblocker.User;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractFilter {
    
    private final Filters parent;
    
    // cache to prevent excessive queries to the permissions system
    //                  Object the key of the user
    //                  ||||||  CacheValue stores the permission result and the last context of the user
    private final Cache<Object, CacheValue> permissionCache;
    
    String name;
    
    String[] rawPatterns = {};
    Pattern compiled;
    String patternPrefix = "";
    String patternSuffix = "";
    protected FilterAction denyAction = FilterAction.SOFT_DENY;
    protected FilterAction allowAction = FilterAction.SOFT_ALLOW;
    String bypassPerm = "";
    
    String denyMsg = null;
    
    public AbstractFilter(Filters parent, String name) {
        this.parent = parent;
        setName(name);
        resetPatternPrefix();
        resetPatternSuffix();
        resetPermission();
        permissionCache = CacheBuilder.newBuilder()
                .expireAfterWrite(parent.getPermissionCacheMillis(), TimeUnit.MILLISECONDS).build();
    }
    
    public abstract FilterAction apply(User u, String command);
    
    public Matcher getMatcher(String command) {
        return compiled.matcher(command);
    }
    
    public boolean checkPermission(User u) {
        final String perm = getPermission();
        if (perm != null) {
            final CacheValue cached = permissionCache.getIfPresent(u.getKey());
            if (cached != null && u.getContext() == cached.context) { // return cached value if it exists & is the same server
                return cached.result;
            } else if (u.hasPermission(perm)) { // otherwise check perm and cache result
                permissionCache.put(u.getKey(), new CacheValue(true, u.getContext()));
                return true;
            } else {
                permissionCache.put(u.getKey(), new CacheValue(false, u.getContext()));
                return false;
            }
        }
        return true;
    }
    
    public Filters getParent() {
        return parent;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Pattern getPattern() {
        return compiled;
    }
    
    public String[] getPatterns() {
        return Arrays.copyOf(rawPatterns, rawPatterns.length);
    }
    
    public void setPattern(String... regexes) {
        synchronized (this) {
            this.rawPatterns = Arrays.copyOf(regexes, regexes.length);
            recompile();
        }
    }
    
    public FilterAction getDenyAction() {
        return denyAction;
    }
    
    public FilterAction getAllowAction() {
        return allowAction;
    }
    
    public void setDenyAction(FilterAction denyAction) {
        if (denyAction == null) {
            throw new NullPointerException("denyAction is null");
        }
        this.denyAction = denyAction;
    }
    
    public void setAllowAction(FilterAction allowAction) {
        if (allowAction == null) {
            throw new NullPointerException("allowAction is null");
        }
        this.allowAction = allowAction;
    }
    
    protected void recompile() {
        synchronized (this) {
            StringBuilder sb = new StringBuilder();
            sb.append(patternPrefix);
            sb.append('(');
            sb.append('(');
            if (rawPatterns.length > 0) {
                for (String x : rawPatterns) {
                    sb.append(x).append(")|(");
                }
            } else {
                sb.append(')');
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.deleteCharAt(sb.length() - 1);
            sb.append(')');
            sb.append(patternSuffix);
            this.compiled = Pattern.compile(sb.toString());
        }
    }
    
    public String getPatternPrefix() {
        return patternPrefix;
    }
    
    public void setPatternPrefix(String patternPrefix) {
        synchronized (this) {
            this.patternPrefix = patternPrefix;
            recompile();
        }
    }
    
    public void resetPatternPrefix() {
        setPatternPrefix(parent.getDefaultPatternPrefix());
    }
    
    public String getPatternSuffix() {
        return patternSuffix;
    }
    
    public void setPatternSuffix(String patternSuffix) {
        synchronized (this) {
            this.patternSuffix = patternSuffix;
            recompile();
        }
    }
    
    public void resetPatternSuffix() {
        setPatternSuffix(parent.getDefaultPatternSuffix());
    }
    
    public String getPermission() {
        return bypassPerm;
    }
    
    public void setPermission(String bypass) {
        this.bypassPerm = bypass;
    }
    
    public void resetPermission() {
        setPermission(parent.getRootFilterPerm() + "." + getName());
    }
    
    public String getDenyMsg() {
        return denyMsg == null ? parent.defaultDenyMsg() : denyMsg;
    }
    
    public void setDenyMsg(String denyMsg) {
        this.denyMsg = denyMsg;
    }
    
    public static class CacheValue {
        public final boolean result;
        public final Object context;
        public CacheValue(boolean result, Object context) {
            this.result = result;
            this.context = context;
        }
    }
    
}
