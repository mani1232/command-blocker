package me.taucu.commandblocker.platform.bukkit.listeners;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.mojang.brigadier.suggestion.Suggestion;
import com.mojang.brigadier.suggestion.Suggestions;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.bukkit.BukkitCommandBlocker;
import me.taucu.commandblocker.platform.bukkit.BukkitUser;

import java.util.Iterator;

public class PacketTabResponseListener extends PacketAdapter {

    final Filters filters;

    public PacketTabResponseListener(BukkitCommandBlocker plugin) {
        super(PacketAdapter.params(plugin, PacketType.Play.Server.TAB_COMPLETE).optionAsync());
        filters = plugin.getFilters();
    }

    public void onPacketSending(PacketEvent e) {
        if (!e.isCancelled()) {
            Suggestions suggestions = e.getPacket().getSpecificModifier(Suggestions.class).read(0);
            boolean change = false;

            if (suggestions.getRange().getStart() < 2) {
                Iterator<Suggestion> it = suggestions.getList().iterator();
                BukkitUser u = new BukkitUser(e.getPlayer());
                while (it.hasNext()) {
                    Suggestion suggest = it.next();
                    if (filters.apply(u, "/" + suggest.getText()) != null) {
                        it.remove();
                        change = true;
                    }
                }
            }

            if (change) {
                if (suggestions.getList().isEmpty()) {
                    e.setCancelled(true);
                }
                e.getPacket().getSpecificModifier(Suggestions.class).write(0, suggestions);
            }
        }
    }

    public void register() {
        ProtocolLibrary.getProtocolManager().addPacketListener(this);
    }

    public void unregister() {
        ProtocolLibrary.getProtocolManager().removePacketListener(this);
    }

}
