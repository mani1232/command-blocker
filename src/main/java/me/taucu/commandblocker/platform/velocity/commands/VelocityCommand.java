package me.taucu.commandblocker.platform.velocity.commands;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.tree.LiteralCommandNode;
import com.velocitypowered.api.command.CommandSource;
import com.velocitypowered.api.proxy.ConsoleCommandSource;
import me.taucu.commandblocker.platform.AbstractCommandBlocker;
import me.taucu.commandblocker.util.logging.LogUtils;
import me.taucu.commandblocker.util.logging.SimpleLogFormatter;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;

public class VelocityCommand {

    public final AbstractCommandBlocker plugin;

    public final LiteralCommandNode<CommandSource> root;

    public VelocityCommand(AbstractCommandBlocker plugin) {
        this.plugin = plugin;

        this.root = LiteralArgumentBuilder.<CommandSource>literal("vcommandblocker")
                .requires(ctx -> ctx.hasPermission("tau.cmdblock.command"))
                .executes(ctx -> {
                    ctx.getSource().sendMessage(Component.text("Usage: /" + ctx.getInput() + " (reload|version)").color(NamedTextColor.RED));
                    return 1;
                })
                .then(LiteralArgumentBuilder.<CommandSource>literal("reload")
                        .executes(ctx -> {
                            CommandSource src = ctx.getSource();
                            src.sendMessage(Component.text("reloading...").color(NamedTextColor.GOLD));
                            if (src instanceof ConsoleCommandSource) {
                                this.plugin.getConfigLoader().reloadConfig();
                            } else {
                                try (LogUtils.LoggerAttachment attach = LogUtils.attachToLogger(plugin.getLogger(), SimpleLogFormatter.INSTANCE, msg -> src.sendMessage(Component.text(msg)))) {
                                    this.plugin.getConfigLoader().reloadConfig();
                                }
                            }
                            src.sendMessage(Component.text("reloaded.").color(NamedTextColor.GREEN));
                            return 1;
                        })
                        .build())
                .then(LiteralArgumentBuilder.<CommandSource>literal("version")
                        .executes(ctx -> {
                            CommandSource src = ctx.getSource();
                            src.sendMessage(Component.text("Running CommandBlocker version: ").color(NamedTextColor.GREEN)
                                    .append(Component.text(plugin.getPluginVersion()).color(NamedTextColor.GOLD))
                            );
                            return 1;
                        })
                        .build())
                .build();
    }

}
