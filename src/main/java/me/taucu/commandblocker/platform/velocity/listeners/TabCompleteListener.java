package me.taucu.commandblocker.platform.velocity.listeners;

import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.player.TabCompleteEvent;
import me.taucu.commandblocker.User;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.velocity.VelocityCommandBlocker;
import me.taucu.commandblocker.platform.velocity.VelocityUser;

import java.util.Iterator;

public class TabCompleteListener {

    private final Filters filters;

    public TabCompleteListener(VelocityCommandBlocker plugin) {
        this.filters = plugin.getFilters();
    }

    @Subscribe
    public void onTab(TabCompleteEvent e) {
        User u = new VelocityUser(e.getPlayer());
        if (filters.apply(u, e.getPartialMessage()) != null) {
            e.getSuggestions().clear();
        } else {
            Iterator<String> it = e.getSuggestions().iterator();
            while (it.hasNext()) {
                if (filters.apply(u, e.getPartialMessage() + " " + it.next()) != null) {
                    it.remove();
                }
            }
        }
    }

}
