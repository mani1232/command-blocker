package me.taucu.commandblocker.platform.velocity.listeners;

import com.velocitypowered.proxy.protocol.packet.TabCompleteRequest;
import dev.simplix.protocolize.api.Direction;
import dev.simplix.protocolize.api.listener.PacketReceiveEvent;
import dev.simplix.protocolize.api.listener.PacketSendEvent;
import me.taucu.commandblocker.CommandBlocker;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.util.TabMemory;
import me.taucu.commandblocker.util.packets.ProtocolizePacketListener;

public class PacketTabRequestListener extends ProtocolizePacketListener<TabCompleteRequest> {

    final Filters filters;

    public PacketTabRequestListener(CommandBlocker pl) {
        super(TabCompleteRequest.class, Direction.UPSTREAM, 0);
        this.filters = pl.getFilters();
    }

    @Override
    public void packetReceive(PacketReceiveEvent<TabCompleteRequest> e) {
        TabCompleteRequest req = e.packet();
        // always redefine to ensure an atomic, consistent state
        TabMemory.MEMORY.put(e.player().uniqueId(), new TabMemory(req.getTransactionId(), req.getCommand()));
    }

    @Override
    public void packetSend(PacketSendEvent<TabCompleteRequest> packetSendEvent) {}
}
