package me.taucu.commandblocker.platform.velocity.listeners;

import com.velocitypowered.proxy.protocol.packet.TabCompleteResponse;
import dev.simplix.protocolize.api.Direction;
import dev.simplix.protocolize.api.listener.PacketReceiveEvent;
import dev.simplix.protocolize.api.listener.PacketSendEvent;
import me.taucu.commandblocker.CommandBlocker;
import me.taucu.commandblocker.User;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.velocity.VelocityUser;
import me.taucu.commandblocker.util.TabMemory;
import me.taucu.commandblocker.util.packets.ProtocolizePacketListener;

import java.util.Iterator;

public class PacketTabResponseListener extends ProtocolizePacketListener<TabCompleteResponse> {

    final CommandBlocker pl;
    final Filters filters;

    public PacketTabResponseListener(CommandBlocker pl) {
        super(TabCompleteResponse.class, Direction.UPSTREAM, 0);
        this.pl = pl;
        this.filters = pl.getFilters();
    }

    @Override
    public void packetReceive(PacketReceiveEvent<TabCompleteResponse> packetReceiveEvent) {}

    @Override
    public void packetSend(PacketSendEvent<TabCompleteResponse> e) {
        TabCompleteResponse res = e.packet();
        TabMemory mem = TabMemory.MEMORY.get(e.player().uniqueId());
        if (mem != null) {
            if (res.getTransactionId() == mem.transactionId && mem.request.length() >= res.getStart()) {
                User u = new VelocityUser(e.player().handle());
                String start = mem.request.substring(0, res.getStart());
                Iterator<TabCompleteResponse.Offer> it = res.getOffers().iterator();
                while (it.hasNext()) {
                    if (filters.apply(u, start + it.next().getText()) != null) {
                        it.remove();
                    }
                }

                if (!res.getOffers().isEmpty()) {
                    return; //valid responses
                }
            }
        }
        e.cancelled(true); //nonsense or empty responses
    }

}
