package me.taucu.commandblocker.platform.velocity.listeners;

import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.proxy.protocol.packet.chat.session.SessionPlayerCommand;
import dev.simplix.protocolize.api.Direction;
import dev.simplix.protocolize.api.listener.PacketReceiveEvent;
import dev.simplix.protocolize.api.listener.PacketSendEvent;
import me.taucu.commandblocker.CommandBlocker;
import me.taucu.commandblocker.User;
import me.taucu.commandblocker.filters.AbstractFilter;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.velocity.VelocityUser;
import me.taucu.commandblocker.util.packets.ProtocolizePacketListener;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;

public class PacketCommandListener extends ProtocolizePacketListener<SessionPlayerCommand> {

    final Filters filters;

    final LegacyComponentSerializer serializer = LegacyComponentSerializer.legacyAmpersand();

    public PacketCommandListener(CommandBlocker pl) {
        super(SessionPlayerCommand.class, Direction.UPSTREAM, 0);
        this.filters = pl.getFilters();
    }

    @Override
    public void packetReceive(PacketReceiveEvent<SessionPlayerCommand> e) {
        SessionPlayerCommand chat = e.packet();
        User u = new VelocityUser(e.player().handle());
        AbstractFilter result = filters.apply(u, "/" + chat.getCommand());
        if (result != null) {
            e.cancelled(true);
            ((Player) e.player().handle()).sendMessage(serializer.deserialize(result.getDenyMsg()));
        }
    }

    @Override
    public void packetSend(PacketSendEvent<SessionPlayerCommand> packetSendEvent) {}

}
