package me.taucu.commandblocker.platform.bungeecord.listeners;

import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.bungeecord.BungeeCommandBlocker;
import me.taucu.commandblocker.platform.bungeecord.BungeeUser;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.Iterator;

public class TabCompleteListener implements Listener {
    
    private final Filters filters;
    
    public TabCompleteListener(BungeeCommandBlocker pl) {
        this.filters = pl.getFilters();
    }
    
    @EventHandler
    public void onTab(TabCompleteEvent e) {
        if (e.getSender() instanceof ProxiedPlayer) {
            BungeeUser u = new BungeeUser((ProxiedPlayer) e.getSender());
            if (filters.apply(u, e.getCursor()) != null) {
                e.setCancelled(true);
            } else {
                Iterator<String> it = e.getSuggestions().iterator();
                while (it.hasNext()) {
                    if (filters.apply(u, e.getCursor() + " " + it.next()) != null) {
                        it.remove();
                    }
                }
            }
        }
    }
    
}
