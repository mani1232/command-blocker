package me.taucu.commandblocker.platform.bungeecord;

import me.taucu.commandblocker.User;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class BungeeUser implements User {

    public final ProxiedPlayer player;

    public BungeeUser(ProxiedPlayer player) {
        this.player = player;
    }

    @Override
    public boolean hasPermission(String permission) {
        return player.hasPermission(permission);
    }

    @Override
    public Object getKey() {
        return player.getUniqueId();
    }

    @Override
    public Object getContext() {
        return player.getServer();
    }

}
