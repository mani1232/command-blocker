package me.taucu.commandblocker.platform.bungeecord.commands;

import me.taucu.commandblocker.platform.bungeecord.BungeeCommandBlocker;
import me.taucu.commandblocker.util.logging.LogUtils;
import me.taucu.commandblocker.util.logging.LogUtils.LoggerAttachment;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;
import net.md_5.bungee.command.ConsoleCommandSender;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("deprecation")
public class BungeeCommand extends Command implements TabExecutor {
    
    private final BungeeCommandBlocker plugin;

    private final BaseComponent[] reloading = (new ComponentBuilder()).append("reloading config...")
            .color(ChatColor.GOLD).create();
    private final BaseComponent[] reloaded = (new ComponentBuilder()).append("reloaded!").color(ChatColor.GREEN)
            .create();
    
    public BungeeCommand(BungeeCommandBlocker plugin, String... aliases) {
        super("bcommandblocker", "tau.cmdblock.command", aliases);
        this.plugin = plugin;
    }
    
    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!sender.hasPermission(getPermission())) {
            sender.sendMessage(plugin.getFilters().defaultDenyMsg());
            return;
        }
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("reload")) {
                sender.sendMessage(reloading);
                if (sender instanceof ConsoleCommandSender) {
                    plugin.getConfigLoader().reloadConfig();
                } else {
                    try (LoggerAttachment attach = LogUtils.attachToLogger(plugin.getLogger(), sender::sendMessage)) {
                        plugin.getConfigLoader().reloadConfig();
                    }
                }
                sender.sendMessage(reloaded);
            } else if (args[0].equalsIgnoreCase("version")) {
                sender.sendMessage(new ComponentBuilder("Running CommandBlocker version: ").color(ChatColor.GREEN).append(plugin.getPluginVersion()).color(ChatColor.GOLD).create());
            }
        } else {
            sender.sendMessage("Usage: /bcommandblocker reload");
        }
    }
    
    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] a) {
        if (a.length > 1) {
            return Arrays.asList("");
        } else {
            return Stream.of("reload", "version").filter(s -> a.length == 0 || s.toLowerCase().contains(a[0].toLowerCase())).collect(Collectors.toList());
        }
    }
    
}
