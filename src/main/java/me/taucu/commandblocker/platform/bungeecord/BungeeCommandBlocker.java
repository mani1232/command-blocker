package me.taucu.commandblocker.platform.bungeecord;

import me.taucu.commandblocker.CommandBlocker;
import me.taucu.commandblocker.ConfigLoader;
import me.taucu.commandblocker.filters.Filters;
import me.taucu.commandblocker.platform.bungeecord.commands.BungeeCommand;
import me.taucu.commandblocker.platform.bungeecord.listeners.*;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;

import java.io.File;
import java.util.Properties;

public class BungeeCommandBlocker extends Plugin implements CommandBlocker {

    private final Properties pluginProperties = loadInternalProperties("command-blocker.properties", new Properties());
    private final Filters filters = new Filters();
    
    private final ConfigLoader configLoader = new ConfigLoader(getDataFolder(), "config.yml", getLogger(), filters);

    private PacketDeclareCommandsListener packetDeclareCommandsListener = null;
    private PacketTabRequestListener packetTabRequestListener = null;
    private PacketTabResponseListener packetTabResponseListener = null;

    @Override
    public void onEnable() {
        registerCommands();
        configLoader.reloadConfig();
        registerEvents(new CommandListener(this));
        registerEvents(new TabCompleteListener(this));
        if (getProxy().getPluginManager().getPlugin("Protocolize") != null) {
            registerEvents(new PlayerListener()); // used to clean up TabMemory
            this.packetDeclareCommandsListener = new PacketDeclareCommandsListener(this).register();
            this.packetTabRequestListener = new PacketTabRequestListener(this).register();
            this.packetTabResponseListener = new PacketTabResponseListener(this).register();
        } else {
            getLogger().warning("Protocolize not found; Protocolize is required to prevent listing and tab-completion of commands");
        }
    }
    
    @Override
    public void onDisable() {
        if (packetDeclareCommandsListener != null) {
            packetDeclareCommandsListener.unregister();
        }
        if (packetTabRequestListener != null) {
            packetTabRequestListener.unregister();
        }
        if (packetTabResponseListener != null) {
            packetTabResponseListener.unregister();
        }
    }

    @Override
    public File getDataDirectory() {
        return getDataFolder();
    }

    @Override
    public ConfigLoader getConfigLoader() {
        return configLoader;
    }

    @Override
    public Filters getFilters() {
        return filters;
    }

    @Override
    public String getPluginVersion() {
        return getDescription().getVersion();
    }

    @Override
    public Properties getPluginProperties() {
        return pluginProperties;
    }

    public void registerCommands() {
        getProxy().getPluginManager().registerCommand(this, new BungeeCommand(this, parseAliases("bungee-command-aliases", "bcmdblocker bcmdblock bcb")));
    }
    
    public <T extends Listener> T registerEvents(T l) {
        ProxyServer.getInstance().getPluginManager().registerListener(this, l);
        return l;
    }
    
}
