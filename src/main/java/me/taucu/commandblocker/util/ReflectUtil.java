package me.taucu.commandblocker.util;

import java.lang.reflect.Field;

public abstract class ReflectUtil {

    public static Field fuzzyFindDeclaredFieldByType(Class<?> target, Class<?> type) throws ReflectiveOperationException {
        try {
            for (Field f : target.getDeclaredFields()) {
                if (type.isAssignableFrom(f.getType())) {
                    return f;
                }
            }
        } catch (Exception e2) {
            throw new ReflectiveOperationException("Failed to find field by type \"" + type + "\" in \"" + target + "\"", e2);
        }
        throw new NoSuchFieldException("Could not find a field by the type of \"" + type + "\" in \"" + target + "\"");
    }

}
