package me.taucu.commandblocker;

import me.taucu.commandblocker.filters.Filters;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public interface CommandBlocker {

    File getDataDirectory();

    ConfigLoader getConfigLoader();

    Filters getFilters();

    Logger getLogger();

    String getPluginVersion();

    Properties getPluginProperties();

    default Properties loadInternalProperties(String path, Properties into) {
        into.clear();
        InputStream input = getClass().getClassLoader().getResourceAsStream(path);
        if (input == null) {
            getLogger().log(Level.SEVERE, "Failed to find properties: " + path);
        } else {
            try (InputStream in = input) {
                into.load(in);
            } catch (IOException | IllegalArgumentException | NullPointerException e) {
                getLogger().log(Level.SEVERE, "Failed to load properties: " + path, e);
            }
        }
        return into;
    }

    default String[] parseAliases(String path, String defaults) {
        String property = getPluginProperties().getProperty(path, defaults);
        return Arrays.stream(property.split("\\s+")).filter(s -> {
            if (s.matches("[a-zA-Z0-9]+")) {
                return s.length() > 0;
            } else {
                getLogger().severe("Alias \"" + s + "\" does not match [a-zA-Z0-9]");
                return false;
            }
        }).toArray(String[]::new);
    }


}
