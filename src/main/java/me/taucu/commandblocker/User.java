package me.taucu.commandblocker;

public interface User {

    public boolean hasPermission(String permission);

    public Object getKey();

    public Object getContext();

}
